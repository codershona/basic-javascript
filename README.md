# Basic Javascript

##  Javascript Algorithms and Data Structures Certification Study


1) Basic Javascript Part 1 and 2;
2) ES6;
3) Regular Expressions;
4) Debugging;
5) Basic Data Structures;
6) Basic Algorithm Scripting;
7) OOP;
8) Functional Programming;
9) Intermidiate Algorithm Scripting;
10) Projects
11) Javascript Array and Object Manipulation method